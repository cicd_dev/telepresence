k config get-contexts 
k config use-context dev


curl -ik https://kubernetes.default

k create ns telepresence
kubectl config set-context --current --namespace telepresence
skaffold delete
skaffold run --default-repo remotejob

kubectl port-forward deployment.apps/web 3000:3000

kubectl port-forward pod/svc1-5dccdc5cff-p4k6q 8090:8090


sudo curl -fL https://app.getambassador.io/download/tel2/linux/amd64/latest/telepresence -o /usr/local/bin/telepresence

sudo chmod a+x /usr/local/bin/telepresence

telepresence connect

telepresence intercept svc1 --port 3001 --ingress-host dev --ingress-port 8090


telepresence uninstall -a

telepresence helm uninstall

telepresence status